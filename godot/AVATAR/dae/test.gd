extends Spatial

var accumul = 0
var neck_rangez = 1.3
var neck_rangex = -0.5
var jaw_rangex = 0.4

func _ready():
	pass

func _process(delta):
	rotate_y( -delta * 0.45142 )
	var skeleton = get_node("character_export01/Skeleton")
	
	accumul += delta
	var s = ( 1 - sin( accumul ) ) * 0.5
	var neckz = -( neck_rangez * 0.5 ) + s * neck_rangez
	var neckx = -( neck_rangex * 0.5 ) + s * neck_rangex
	var jawx = s * jaw_rangex
	
	var neck01_id = skeleton.find_bone( "neck01" )
	var neck02_id = skeleton.find_bone( "neck02" )
	var neck03_id = skeleton.find_bone( "neck03" )
	var jaw_id = skeleton.find_bone( "jaw" )
	#var t = skeleton.get_bone_pose( neck03_id )
	var t = null
	t = Transform().rotated( Vector3(0,0,1), neckz * 0.2 )
	t = t.rotated( Vector3(1,0,0), neckx * 0.2 )
	skeleton.set_bone_pose( neck01_id, t )
	t = Transform().rotated( Vector3(0,0,1), neckz * 0.5 )
	t = t.rotated( Vector3(1,0,0), neckx * 0.5 )
	skeleton.set_bone_pose( neck02_id, t )
	t = Transform().rotated( Vector3(0,0,1), neckz )
	t = t.rotated( Vector3(1,0,0), neckx )
	skeleton.set_bone_pose( neck03_id, t )
	t = Transform().rotated( Vector3(1,0,0), jawx )
	skeleton.set_bone_pose( jaw_id, t )
	pass