## notes de réunion 12/07/2018

Droits

UMONS renverra un mail pour dire "ok pour tout mettre en MIT/CC0" = license la plus permissive. Les headers seront copyright UMONS, et citeront les auteurs

Plateforme

À mettre sur plateforme GITHUB numediart : Kevin crée le projet et met FZ et Mickael Boitte comme utilisateurs.
* GG mettra les outils du GITHUB sur le site numédiart UMONS.
* FZ pointera sur la page UMONS du projet dans sa comm. 

Communication sur le projet

Ils communiquent sur le projet en construction sur leur facebook avec @numédiart (mais pas  @umons) ET sur leur site polymorph.

Autre détails

Délai d'execution : 200 jours? Redemander à Kevin.
Pas de maintenance prévue pour recompiler les libs et l'exécutable sous Win, OSX, Linux (Debian prévu).
Pas de temps prévu pour présenter le projet dans des confs, mais sur le plan scientifique ca se fera via le labo.
Facturation : je demande si 10% upfront = acceptable

 

Planning
* Tranche 2 31/8
* Tranche 3 15/9
* Tranche 4 15/11

+ livraison beta 3/12 et début de comm.

* Tranche 5  validation 15/12 : mise en ligne de la version 1 pour téléchargement.

Documentation Doxygen
Homme ou femme? Androgyne… voir avec Kevin.
Age?

Tournera dans Godot
= engine OS; Pas Unity ou Blender! Si il le faut, il faut écrire un wrapper pour chaque moteur!
Demander à Kevin!!!
Blender = moteur 3D arrive dans blender, mais pas diffusion d'exécutable temps réel aussi facile que dans Godot.
Godot = le unity OpenSource

## mail de kevin, 12/07/2018

1- licence: MIT est très bien je trouve

2-plateform: ok je ferais ça cette semaine comme ça c'est deja pret.

3-nombre de bones: l'idée de base etait d'avoir un avatar auquel on peut "plugger" plusieurs source de control et donc avoir le control a plusieurs niveaux: example - low level comme des coordonnées landmarks venant d'open face ou de kinect, optitract (plus de points), ou high level comme des commandes a partir de mots clef (smile, laugh, angry, happy, etc.), ou des niveau intermedieres comme des action units, etc.

Donc le plus de nombre de bones possible le mieu je crois, et un point important pour moi ce serait d'avoir les plis de la peau au niveau des joues et des yeux (qui caracterisent par example les sourirs et les rires). Qu'en pensez-vous?

4-avatar: En prenant en compte le point 3 et en gardant en tete que le but est que l'avatar soit open source et le plus maleable possible, je propose ce qui suit:
Si on peut avoir un Androgyne en 200 jours a qui on peut changer 2/3 features pour que ça tendent plus vers un homme ou une femme (menton, cheveux, etc...) ce serait l'ideal. Si on est obligé de choisir, je preferais une femme.
Par rapport a la forme, je ne sais pas ce qui est possible, mais je pencherais plus pour un style https://www.pinterest.com/pin/366199013424011610/ que https://www.youtube.com/watch?v=8qeOFibRmoo parce que ça reste realiste et je trouve cette forme plus acceuillante et plus facile a interragir avec. En plus apres avoir lu quelque papier essailler de copier à 100% un etre humain pour un agent virutel semble ne pas etre la meilleur idée... Mais le plus important reste pour moi la qualite de l'expressivité (j'attend vos feedback).

5- Par rapport à l'integration le but est que le projet "reste en vie" et qu'on arrive a avoir une communauté de contributeurs (on pourra pense a organiser des workshops, competition dans des conf, etc.. autour de l'avatar). Je comprend l'importance du C++ mais est-ce que je pourrais avoir plus de détails par rapport au fichier JSON? C'est juste pour le control de l'avatar et pas pour la partie dev derrière c'est bien ça? Et combien est-ce que ce serait complique d'avoir une sorte de wrapper ou debut de wrapper en plus en python?

Et comme "Pas de maintenance prévue pour recompiler les libs et l'exécutable sous Win, OSX, Linux (Debian prévu)" est-ce qu'il y aura, pendant ces 200 jours, du temps de prevu pour repondre aux questions idotes que j'aurais parfois (genre mettings, skypes, etc...)? Parce que je ne suis pas un pro en C++ (je m'y met!) mais faut qu'en meme que je comprenne ce qui se passe.

6- Par rapport à Godot et un wrapper potentiel, ça me va.

7-Planning: a priori ok en ce qui me concerne, mais je ne serais de retour à Mons qu'en fin Septembre donc un peu avat la fin de la tranche 4. Du coup serait-il possible d'organiser des skypes toutes les 2 semaines en commencant dès la semaine prochaine? (je serais bien presents pendant ces fois-ci et ne ferais plus d'erreurs idotes comme aujourd'hui avec le changement d'heures et me fierais plus a mon calendar).

## references

### cg

* http://www.cgsociety.org/training/course/character-facial-rigging-for-production
* https://www.blendswap.com/blends/view/4861
* https://www.youtube.com/watch?v=wfL8VD7vGGs

### scientific papers

* https://www.jcmfs.com/article/S1010-5182(10)00102-2/abstract
* http://basjsurge.com/wp-content/uploads/2015/01/5-ANTHROPOMETRIC-FACE-IN-BASRAH.pdf
* https://scholarworks.iupui.edu/bitstream/handle/1805/1980/Green_Thesis.pdf
* http://www-2.rotman.utoronto.ca/facbios/file/FinalPDF.pdf
* https://www.cdc.gov/nchs/data/nhanes/nhanes3/cdrom/nchs/manuals/anthro.pdf
* http://www.brandrelationships.org/wp-content/uploads/Aggarwal_Facing-Dominance_Anthropomorphism-Product-Face-Ratios-and-COnsumer-Preferences_BBR-2015.pdf
* https://www.nap.edu/resource/11815/Anthrotech_report.pdf
* http://utw10503.utweb.utexas.edu/publications/2010/sg_ijcv_june10.pdf
* https://www.researchgate.net/publication/224399737_Crafting_Personalized_Facial_Avatars_Using_Editable_Portrait_and_Photograph_Example
* http://www.academia.edu/19064044/The_Myth_of_the_Androgyne_Homoerotics_in_Oscar_Wilde_s_The_Picture_of_Dorian_Gray

### gender theory

* http://lgbt.wikia.com/wiki/Third_gender